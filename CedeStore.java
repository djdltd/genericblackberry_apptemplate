/*
 * CedeStore.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;


import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.Displayable;
import java.util.*;
import java.io.*;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.util.Persistable;

/**
 * 
 */
class CedeStore extends UiApplication {
    
    private PersistentObject _persisttrial;
    private TrialInfo _trialinfo;
    private boolean _trialversion = true;
    private UserSettingsScreen _userSettingsscreen = new UserSettingsScreen();
    
        
    private MenuItem _optionsMenuItem = new MenuItem("Options", 100, 10) 
    {                
        public void run()
        {
            _userSettingsscreen = new UserSettingsScreen();
            
            UiApplication.getUiApplication().pushModalScreen(_userSettingsscreen);
            
        }
    };
    
    private MenuItem _aboutMenuItem = new MenuItem("About", 100, 10) 
    {                
        public void run()
        {            
            if (_trialversion == true) {
                Dialog.alert( GlobalAppProperties._appnamewithversion + ".7.\n\n7 Day Trial\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");
            } else {
                Dialog.alert(GlobalAppProperties._appnamewithversion + ".99.\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");
            }                
        }
    };
    
    CedeStore() {    
            
            CedeStoreMainScreen screen = new CedeStoreMainScreen();
            pushScreen(screen);
    }

    public static void main(String[] args)
    {
        
        // Create a new instance of the application and start 
        // the application on the event thread.
        CedeStore cedestore  = new CedeStore();
        cedestore.enterEventDispatcher();
    }


    public void activate()
    {
           LicenseandPasswordCheck();
    }

    public void LicenseandPasswordCheck()
    {     
        UiApplication.getUiApplication().invokeLater( new Runnable()
        {
            public void run ()
            {
                
                UserSettings mysettings = new UserSettings();
                
                _userSettingsscreen = new UserSettingsScreen();
                mysettings = _userSettingsscreen.getUserSettings();          
                
                if (mysettings._licenseentered.compareTo("yes") != 0) {
                    UiApplication.getUiApplication().pushModalScreen(new LicenseScreen());                       
                    
                    _userSettingsscreen = new UserSettingsScreen();
                    mysettings = _userSettingsscreen.getUserSettings();                                                 
                    
                    if (mysettings._istrial.compareTo("yes") == 0) {
                        _trialversion = true;
                        loadTrialinfo();
                    } else {
                        _trialversion = false;
                    }
                                        
                } else {
                    if (mysettings._istrial.compareTo("yes") == 0) {
                        _trialversion = true;
                        loadTrialinfo();
                    } else {
                        _trialversion = false;
                    }
                }
                
                // Now check if we need to show the password prompt screen
                Date dtCurrent = new Date();        
                long accesstime = dtCurrent.getTime();
                
                if (accesstime > GlobalAppVars._lastaccesstime) {            
                    pushScreen(new PasswordScreen());
                }                     
            }
        });
    }
    
    public void loadTrialinfo()
    {
        long KEY = GlobalAppProperties._trialinfoKEY;        
        _persisttrial = PersistentStore.getPersistentObject( KEY );
        _trialinfo = (TrialInfo) _persisttrial.getContents();
        if( _trialinfo == null ) {
            _trialinfo = new TrialInfo();
            
            // Set the trial expire time                        
            _persisttrial.setContents( _trialinfo );
            
            Date dtCurrent = new Date();        
            long expiretime = dtCurrent.getTime();                            
            expiretime = expiretime + (((60000) * 60) * 24) * 7;
            
            _trialinfo.expiretime = expiretime;            
            _persisttrial.commit();            
        }
    }

    private class CedeStoreMainScreen extends MainScreen {
                
        // Constructor
        private CedeStoreMainScreen()
        {
            
            setTitle(new LabelField(GlobalAppProperties._appname, LabelField.USE_ALL_WIDTH));
            
            
            addMenuItem(_optionsMenuItem);
            addMenuItem(_aboutMenuItem);
        }
    }


} 
