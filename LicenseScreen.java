
package CedeStore;

import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import java.io.IOException;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import java.util.*;

    public class LicenseScreen extends MainScreen
    {
        private boolean _passwordok = false;
        private TrialInfo _trialinfo;
        private PersistentObject _persisttrial;
        private UserSettingsScreen _userSettingsscreen = new UserSettingsScreen();
        
        private MenuItem _passwordMenuItem = new MenuItem("Enter License Key", 100, 10) 
        {                
            public void run()
            {
                passwordPrompt();                   
            }
        };
        
        private MenuItem _forgotMenuItem = new MenuItem("Exit", 100, 10) 
        {                
            public void run()
            {
                System.exit(0);
            }
        };
        
        // Constructor
        public LicenseScreen()
        {
            setTitle(new LabelField(GlobalAppProperties._appname + " - Licensing", LabelField.USE_ALL_WIDTH));
            add(new LabelField("Please enter your license key."));
                    
            addMenuItem(_passwordMenuItem);
            addMenuItem(_forgotMenuItem);
            //UiApplication.getUiApplication().pushScreen(popup);
            passwordPrompt();                        
        }

        public void setTrialinfo()
        {
            long KEY =  GlobalAppProperties._trialinfoKEY;
            _persisttrial = PersistentStore.getPersistentObject( KEY );
                                    
            _trialinfo = new TrialInfo();
            
            // Set the trial expire time                        
            _persisttrial.setContents( _trialinfo );
            
            Date dtCurrent = new Date();        
            long expiretime = dtCurrent.getTime();                            
            expiretime = expiretime + (((60000) * 60) * 24) * 7;
            
            _trialinfo.expiretime = expiretime;            
            _persisttrial.commit();                        
        }

        public boolean isPasswordok()
        {
            return _passwordok;
        }

        public boolean manualActivate(String strProdkey) {
            
            String strResult = "";
            ManualActivation actscreen = new ManualActivation(strProdkey);                       
            UiApplication.getUiApplication().pushModalScreen(actscreen);
        
            strResult = actscreen.getActivationresult();
            
            if (strResult.compareTo("SUCCESS") == 0) {                
                Dialog.alert("Activation was successful. Thank you for purchasing " + GlobalAppProperties._appname + "!");                                           
                return true;
            }
            
            if (strResult.compareTo("FAILED") == 0) {
                Dialog.alert("Activation was not successful. Please contact CedeSoft.");     
                return false;                                      
            }

            if (strResult.compareTo("DENIED") == 0) {
                Dialog.alert("Activation denied. This license has already been activated. If this is a new product purchase, please contact CedeSoft.");   
                return false;                                        
            }     
            
            return false;
        }

        public String activateProductkey(String strProdkey) {
            Licensing lic = new Licensing();
            String strData = "";
            String strActivationurl = "http://www.cedesoft.com/ProdAct147770747.aspx?cm=AAAA&pk=" + strProdkey;
            
            String result = "";
            
            try {
                strData = lic.AutoActivate (strActivationurl);            
                
                if (strData.length() > 30) {
                    
                    if (strData.indexOf("PSALMS118:8") > 2) {
                        
                        if (strData.indexOf("777777714700014700000007777777") > 7) {
                            result = "AUTOSUCCESS-ACTIVATED"; // Activation successful
                        }
                        
                        if (strData.indexOf("777777714800014800000007777777") > 7) {
                            result = "AUTOSUCCESS-FAILED"; // Manual activation necessary
                        }
                        
                        if (strData.indexOf("777777714900014900000007777777") > 7) {
                            result = "AUTOSUCCESS-DENIED"; // Product key already activated - deny
                        }
                        
                    } else {
                        result = "AUTOFAIL"; // Manual activation necessary
                    }
                    
                } else {
                    result = "AUTOFAIL"; // Manual activation necessary
                }
            }
            catch (IOException ie) {                
                result = "AUTOFAIL"; // Manual activation necessary
            }
            
            return result;
            
            // Outputs:
            // AUTOFAIL
            // AUTOSUCCESS-ACTIVATED
            // AUTOSUCCESS-FAILED
            // AUTOSUCCESS-DENIED
        }

        public void passwordPrompt()
        {            
            UiApplication.getUiApplication().invokeLater( new Runnable()
            {
                public void run ()
                {
                    // get the current user settings
                    _userSettingsscreen = new UserSettingsScreen();
                    UserSettings settings = new UserSettings();
                    settings = _userSettingsscreen.getUserSettings();
                    
                    Licensing lic = new Licensing();
                    String strCode  = "";
                    LicensePopupScreen popup = new LicensePopupScreen(false);
                    
                    UiApplication.getUiApplication().pushModalScreen(popup);
                    
                    if (popup.getEscaped() == false) {
                        strCode = popup.getPassword();
                        
                        strCode = strCode.toUpperCase();
                        
                        if (strCode.compareTo(settings._licensekey) != 0) {
                            if (lic.RecoverKey(strCode) == true) {
                                
                                if (lic.isGenuine() == true) {
                                    if (lic.isCorrectsku() == true) {
                                        
                                        if (lic.isTrial() == true) {
                                            // This is a trial key
                                            _passwordok = true;
                                            setTrialinfo();
                                            settings._licenseentered = "yes";
                                            settings._licensekey = strCode;
                                            settings._istrial = "yes";
                                            _userSettingsscreen.setUserSettings(settings);
                                            UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());    
                                            
                                        } else {
                                            // This is a full key
                                            _passwordok = true;
                                            boolean activatedok = false;
                                            
                                            // If this is a full key this is where we would now activate the product online by checking if this key has already
                                            // been activated by calling our cedesoft product activation page and reading the response. If the response is
                                            // ok, and it has not already been activated then we proceed with the code below to set the key and turn off trial mode
                                            String actResult = activateProductkey(strCode);
                                            
                                            // AUTOFAIL
                                            // AUTOSUCCESS-ACTIVATED
                                            // AUTOSUCCESS-FAILED
                                            // AUTOSUCCESS-DENIED
                                            
                                            if (actResult.length() > 0) {
                                                if (actResult.compareTo("AUTOFAIL") == 0) {                                                    
                                                    // Activate manually
                                                   activatedok = manualActivate(strCode);
                                                }
                                                
                                                if (actResult.compareTo("AUTOSUCCESS-ACTIVATED") == 0) {
                                                    activatedok = true;
                                                    Dialog.alert("Activation was successful. Thank you for purchasing " + GlobalAppProperties._appname + "!");                                           
                                                }
                                                
                                                if (actResult.compareTo("AUTOSUCCESS-FAILED") == 0) {
                                                    // Activate manually                                                
                                                    activatedok = manualActivate(strCode);
                                                }
                                                
                                                if (actResult.compareTo("AUTOSUCCESS-DENIED") == 0) {
                                                    // Activation denied - key already activated
                                                    Dialog.alert("Activation denied. This license has already been activated. If this is a new product purchase, please contact CedeSoft.");                                           
                                                }
                                                
                                            } else {
                                                // Activate manually
                                                activatedok = manualActivate(strCode);
                                            }
                                            
                                            if (activatedok == true) {
                                                settings._licenseentered = "yes";
                                                settings._licensekey = strCode;
                                                settings._istrial = "no";
                                                _userSettingsscreen.setUserSettings(settings);                                            
                                                UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());    
                                            } 
                                        }
                                        
                                    } else {
                                        Dialog.alert("This license key is for a different CedeSoft product. Please contact CedeSoft.");
                                    }
                                } else {
                                    Dialog.alert("License key not authorised. Please contact CedeSoft.");
                                }
                            } else {
                                Dialog.alert("Invalid License Key");
                            }
                        } else {
                            Dialog.alert("This license key is already in use.");
                        }                                                                                               
                    } else {
                        Dialog.alert("You must enter your license key to proceed.");
                    }
                }
            } );
        }
        
        
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            //super.close();
            
        }
    }

