/*
 * GlobalAppVars.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;




/**
 * 
 */
class GlobalAppVars {
    
    public static long _lastaccesstime;
    public static String _encryptedpassword;
    public static boolean _encryptedpasswordpresent;
    
    GlobalAppVars() {    }
} 
