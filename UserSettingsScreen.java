/*
 * UserSettingsScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;

import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.LED;


/**
 * 
 */
class UserSettingsScreen extends MainScreen {
    
    
    private ObjectChoiceField _efchoice;
    
    private UserSettings _settings = new UserSettings();
    private PersistentObject _persist;
    String[] strchoices = new String[8];
    
    
    private MenuItem _saveMenuItem = new MenuItem("Save", 100, 10) 
    {
        public void run()
        {
            setLocalsettings();
            _persist.commit();
                        
            UiApplication uiapp = UiApplication.getUiApplication();
            uiapp.popScreen(uiapp.getActiveScreen());            
        }
    };
     
    UserSettingsScreen() {    
    
        setTitle(new LabelField("CedeStore Options" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
                
        strchoices[0] = "None";
        strchoices[1] = "1 min";
        strchoices[2] = "5 min";
        strchoices[3] = "10 min";
        strchoices[4] = "20 min";
        strchoices[5] = "30 min";
        strchoices[6] = "1 hour";
        strchoices[7] = "2 hours";
                
        _efchoice = new ObjectChoiceField("Password prompt grace period: ", strchoices);
        
        add(_efchoice);
        
        addMenuItem(_saveMenuItem);
        
        loadSettings();
        setLocalform();
        
    }
    
    public boolean onSave()
    {
        setLocalsettings();
        _persist.commit();
        return true;
    }
           
    public void loadSettings ()
    {
         // Hash of "CedeStore".
         //7b25c9e033a66dc3010c12604d7d5c81
         long KEY =  GlobalAppProperties._settingsKEY;
         
         _persist = PersistentStore.getPersistentObject( KEY );
         _settings = (UserSettings) _persist.getContents();
         if( _settings == null ) {
             _settings = new UserSettings();
             _persist.setContents( _settings );
             //persist.commit()
         }
    }
    
    
    public void setLocalsettings()
    {
        _settings._passwordgrace = getGracechoice();        
    }
    
    public void setLocalform()
    {
        setGracechoice(_settings._passwordgrace);       
    }
    
    public void setGracechoice(String strChoice)
    {              
        if (strChoice.trim() == "") {
            _efchoice.setSelectedIndex(0);
        } else {
            
            int c = 0;
            String strcur = "";
            
            for(c=0;c<strchoices.length;c++)
            {
                strcur = strchoices[c];
                if (strcur.compareTo(strChoice) == 0) {
                    _efchoice.setSelectedIndex(c);
                    return;
                }
            }            
        }
    }
    
    public String getGracechoice()
    {
        int index = _efchoice.getSelectedIndex();
        
        return strchoices[index];
    }
    
    public void setMultichoice(ObjectChoiceField myfield, String strChoice, String[] mychoices)
    {
        if (strChoice.trim() == "") {
            myfield.setSelectedIndex(0);
        } else {
            
            int c = 0;
            String strcur = "";
            
            for(c=0;c<mychoices.length;c++)
            {
                strcur = mychoices[c];
                if (strcur.compareTo(strChoice) == 0) {
                    myfield.setSelectedIndex(c);
                    return;
                }
            }            
        }
    }
    
    public String getMultichoice (ObjectChoiceField myfield, String[] mychoices)
    {
        int index = myfield.getSelectedIndex();
        
        return mychoices[index];
    }
    
    
    public UserSettings getUserSettings()
    {
        return _settings;
    }
    
    public void setUserSettings (UserSettings settings)
    {
        _settings = settings;
        _persist.commit();
    }
    
    public void clearSettings ()
    {               
        _settings._initialsetupcompleted = "";        
        _settings._passwordgrace = "";
        _settings._encryptedtag = "";        
                
        _persist.commit();
    }
}
 

