/*
 * PasswordScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;


import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;

import javax.microedition.io.*;
import javax.microedition.lcdui.Displayable;
import java.util.*;
import java.io.*;

 public class PasswordScreen extends MainScreen
    {                                
        private MenuItem _passwordMenuItem = new MenuItem("Enter Password", 100, 10) 
        {                
            public void run()
            {
                passwordPrompt();                   
            }
        };
        
        private MenuItem _forgotMenuItem = new MenuItem("Forgot Password", 100, 10) 
        {                
            public void run()
            {
                forgotPassword();               
            }
        };
        
        // Constructor
        public PasswordScreen()
        {
            setTitle(new LabelField(GlobalAppProperties._appname + " - Authorisation", LabelField.USE_ALL_WIDTH));
            add(new LabelField("Your password is required before you can gain access to " + GlobalAppProperties._appname + "."));
                    
            addMenuItem(_passwordMenuItem);
            addMenuItem(_forgotMenuItem);
            //UiApplication.getUiApplication().pushScreen(popup);
            passwordPrompt();                        
        }

        public void forgotPassword()
        {
            if (Dialog.ask(Dialog.D_YES_NO, "To reset your password you must reset " + GlobalAppProperties._appname + " to factory defaults erasing all data. Do you wish to proceed?") == Dialog.YES) {
                
                
                //clearSMSlist();
                
                UserSettingsScreen userSettingsscreen;
                
                userSettingsscreen = new UserSettingsScreen();
                userSettingsscreen.clearSettings();
                
                passwordPrompt();
            }            
        }

        public int getGracesetting()
        {
            //strchoices[0] = "None";
            //strchoices[1] = "1 min";
            //strchoices[2] = "5 min";
            //strchoices[3] = "10 min";
            //strchoices[4] = "20 min";
            //strchoices[5] = "30 min";
            //strchoices[6] = "1 hour";
            //strchoices[7] = "2 hours";
            
            UserSettingsScreen usersettings = new UserSettingsScreen();
            UserSettings settings = usersettings.getUserSettings();
            
            if (settings._passwordgrace.trim() == "") {
                return 0;
            } else {
                if (settings._passwordgrace.compareTo("None") == 0) {
                    return 0;
                }
                
                if (settings._passwordgrace.compareTo("1 min") == 0) {
                    return 1;
                }
                
                if (settings._passwordgrace.compareTo("5 min") == 0) {
                    return 5;
                }
                
                if (settings._passwordgrace.compareTo("10 min") == 0) {
                    return 10;
                }
                
                if (settings._passwordgrace.compareTo("20 min") == 0) {
                    return 20;
                }
                
                if (settings._passwordgrace.compareTo("30 min") == 0) {
                    return 30;
                }
                                
                if (settings._passwordgrace.compareTo("1 hour") == 0) {
                    return 60;
                }
                
                if (settings._passwordgrace.compareTo("2 hours") == 0) {
                    return 120;
                }
                
                return 0;
            }
        }

        public void passwordPrompt()
        {            
            UiApplication.getUiApplication().invokeLater( new Runnable()
            {
                public void run ()
                {
                    //strDecrypted = Encryption.DecryptTextStrong(strPassword, strFilterencrypted);
                    UserSettingsScreen usersettings = new UserSettingsScreen();
                    UserSettings settings = usersettings.getUserSettings();
                    PasswordPopupScreen popup;
                    Date dtCurrent;
                    long accesstime = 0;
                    
                    boolean initialsetup = false;
                    
                    if (settings._initialsetupcompleted.compareTo("yes") == 0) {
                        popup = new PasswordPopupScreen(false);
                        initialsetup = false;
                    } else {
                        popup = new PasswordPopupScreen(true);
                        initialsetup = true;
                    }                                        
                    
                    UiApplication.getUiApplication().pushModalScreen(popup);
                    
                    if (popup.getEscaped() == false) {
                    
                        if (initialsetup == true) {           
                            settings._encryptedtag = Encryption.EncryptTextEx(popup.getPassword(), "PSALMS118V8");
                            settings._initialsetupcompleted = "yes";                        
                            GlobalAppVars._encryptedpassword = Encryption.Instantencrypt(popup.getPassword());
                            GlobalAppVars._encryptedpasswordpresent = true;
                                                        
                            usersettings.setUserSettings(settings);
                            
                            dtCurrent = new Date();        
                            accesstime = dtCurrent.getTime();                            
                            GlobalAppVars._lastaccesstime = accesstime + ((60000) * getGracesetting());
                            
                            UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());    
                        } else {                                                                  
                            if (Encryption.DecryptTextEx(popup.getPassword(),settings._encryptedtag).compareTo("PSALMS118V8") == 0) {
                                GlobalAppVars._encryptedpassword = Encryption.Instantencrypt(popup.getPassword());
                                GlobalAppVars._encryptedpasswordpresent = true;                                
                                
                                dtCurrent = new Date();        
                                accesstime = dtCurrent.getTime();                            
                                GlobalAppVars._lastaccesstime = accesstime + ((60000) * getGracesetting());
                                
                                UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());    
                            } else {
                                Dialog.alert("Invalid password. Access Denied.");
                            }                                      
                        }
                    
                    } else {
                        Dialog.alert("You must enter your password to proceed.");
                    }
                }
            } );
        }
        
        
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            //super.close();
            
        }
    }
