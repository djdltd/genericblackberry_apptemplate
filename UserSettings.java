/*
 * UserSettings.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;

import net.rim.device.api.util.Persistable;


/**
 * 
 */
class UserSettings implements Persistable {
            
    public String _encryptedtag = "";
    public String _initialsetupcompleted = "";
    public String _passwordgrace = "";    
    public String _licenseentered = "";
    public String _licensekey = "";
    public String _istrial = "yes";
    
    UserSettings() {    
    
    }
} 
