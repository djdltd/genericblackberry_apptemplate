/*
 * Licensing.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;

import java.util.*;
import net.rim.device.api.util.StringUtilities;
import javax.microedition.io.*;
import java.io.*;
import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;


/**
 * USAGE FOR Licensing class:
 * 
 * Firstly call RecoverKey() passing in the license key which will decode and check the validity of the key, check the return value which will be
 * true or false;
 * 
 * if RecoverKey is successful then check for a genuine key by calling isGenuine()
 * 
 * if GenuineKey returns true, then check that the recovered key is for the correct product id by calling isCorrectsku().
 * 
 * if the SKU is correct then check for a trial key by calling isTrial().
 * 
 * then for more specific license info, use the following methods:
 * 
 * getTrialdays() - returns the number of days in the trial
 * getMaxclients() - returns the maximum number of clients in this key (if applicable)
 * getVid1() - returns the current distribution id
 * getVid2() - returns the second bank of the distribution id, the second bank should be incremented when the first bank reaches it's max
 * 
 */

 
class Licensing {
    
    
    
    private boolean _genuine = false;
    private boolean _trial = true;
    private int _trialdays = 0;
    private int _sku = 0;
    private int _vid1 = 0;
    private int _vid2 = 0;
    private int _max = 0;
    private static Random _rnd = new Random();
    private ServiceRecord srWAP2;

    private int _mysku = GlobalAppProperties._productSKU; 
     
    public boolean isGenuine()
    {
        return _genuine;
    }
    
    public boolean isTrial()
    {
        return _trial;
    }
    
    public int getTrialdays()
    {
        return _trialdays;
    }
    
    public int getSku()
    {
        return _sku;
    }
    
    public int getVid1()
    {
        return _vid1;
    }
    
    public int getVid2()
    {
        return _vid2;
    }
    
    public boolean isCorrectsku()
    {
        if (_sku == _mysku) {
            return true;
        } else {
            return false;
        }
    }
    
    public int getMaxclients()
    {
        return _max;
    }
           
    public boolean IsNumber(String InputString)
    {
        int c = 0;
        byte[] bInstring = InputString.getBytes();
        
        for (c=0;c<InputString.length();c++) {
            if (bInstring[c] < 48 || bInstring[c] > 57) {
                return false;
            }
        }
        
        return true;
    }    
    
    public String DecCS(String InString, int CSum)
    {
        int c = 0;
        String curchar = "";
        String finalstring = "";
        
        for (c=0;c<InString.length();c++) {
            curchar = InString.substring(c, c+1);                        
            finalstring+=String.valueOf(UnShiftSingle(curchar, CSum));            
            
        }
        
        return finalstring;
    }
       
    public int UnShiftSingle (String InVal, int ShiftAmount)
    {
        int c = 0;
        int curval = Integer.parseInt(InVal);
        
        for (c=0;c<ShiftAmount;c++) {
            curval--;
            if (curval < 0) {
                curval = 9;
            }
        }
        
        return curval;
    }
    
    public String UnShiftPChar (String CharOne, int ShiftAmount)
    {
        int c = 0;
        byte[] bCharOne = CharOne.getBytes();
        int curcode = 0;
        String dec = "";
        
        if (CharOne.length() > 0) {
            curcode = bCharOne[0];
            
            for (c=0;c<ShiftAmount;c++) {
                curcode--;
                
                if (curcode > 57 && curcode <65) {
                    curcode = 57;
                }
                
                if (curcode < 48) {
                    curcode = 90;
                }
            }
            
            bCharOne[0] = (byte) curcode;                        
        }
        
        dec = new String(bCharOne);
        return dec;
    }
    
    
    public String GenCSum(String InString)
    {
        int c = 0;
        int cs = 0;
        
        for (c=0;c<InString.length();c++) {
            cs+=Integer.parseInt(InString.substring(c, c+1));
        }
        
        return String.valueOf(cs);
    }
    
    public String DecString(String InString, String KeyHive)
    {
        int c = 0;
        String curchar = "";
        int curkey = 0;
        String encchar = "";
        String enstring = "";
        
        for (c=0;c<InString.length();c++) {
            
            curkey = Integer.parseInt(KeyHive.substring(c, c+2));
            
            curchar = InString.substring(c, c+1);
            
            encchar = UnShiftPChar(curchar, curkey);
            
            enstring+=encchar;
            
        }
        
        return enstring;
    }
    
    public static long Raise (long lValue, long lPower)
    {
        long lRes = lValue;
        int l=0;
        for (l=1;l<lPower;l++)
        {
            lRes = lRes * lValue;
        }
        return lRes;
    }
    
    public static String Expand (String strInval)
    {
        //add(new RichTextField("Expand function called." ,Field.NON_FOCUSABLE));
        //String strInval = "237745";
        String cExp1 = "";
        String cExp2 = "";
        int c = 0;
        long lExp1;
        long lExp2;
        long lPower=0;
        String strResult = "";
        
        for (c=0;c<strInval.length ();c++)
        {
            if (c>0) 
            {
                cExp1 = strInval.substring(c-1,c);
                cExp2 = strInval.substring(c,c+1);
                lExp1 = Long.parseLong(cExp1);
                lExp2 = Long.parseLong(cExp2);
                
                lPower = Raise(lExp1, lExp2);
                
                strResult += Long.toString(lPower);                
            }
        }
                
        return strResult;
    }

    public static String RepeatExpand (String strInval, int Length)
    {
        String strResult = "";
        String strNewResult = "";
        
        strResult = Expand(strInval);
        
        while (strResult.length() < Length)
        {
            strNewResult = Expand(strResult);
            strResult = strNewResult;
        }
        
        return strResult;
    }
    
    public boolean RecoverKey (String strprodkey)
    {
        if (strprodkey.length() < 3) {
            //System.out.println("License key too short.");
            return false;
        }
        
        String strExpandedKey = RepeatExpand("2347", 77);
        
        //System.out.println("Expanded Key: " + strExpandedKey);
        
        String DString = DecString(strprodkey, strExpandedKey);
        
        //System.out.println("DString: " + DString);
        
        if (IsNumber(DString) == false) {
            //System.out.println("License key invalid. Decoded string not numeric.");
            return false;
        }
        
        String CSum = DString.substring(DString.length()-5);
        String RemCSum = DString.substring(0, DString.length()-5);
        
        //System.out.println("CSum: " + CSum);
        //System.out.println("RemCSum: " + RemCSum);
        
        String DSum = DecCS(RemCSum, Integer.parseInt(CSum));
        
        //System.out.println("DSum: " + DSum);
        
        String Recheckedsum = GenCSum(DSum);
        
        if (Integer.parseInt(CSum) != Integer.parseInt(Recheckedsum)) {
            //System.out.println("Invalid License Key. Checksum mismatch.");
            return false;
        }
        
        int firstauth = Integer.parseInt(DSum.substring(0, 3));
        int trialfull = Integer.parseInt(DSum.substring(3, 5));
        int trialdays = Integer.parseInt(DSum.substring(5, 7));
        int maxclients = Integer.parseInt(DSum.substring(7, 11));
        int productsku = Integer.parseInt(DSum.substring(11, 14));
        int vendid1 = Integer.parseInt(DSum.substring(14, 18));
        int vendid2 = Integer.parseInt(DSum.substring(18, 21));
        
        //System.out.println("First Auth: " + String.valueOf(firstauth));
        //System.out.println("Trial / Full: " + String.valueOf(trialfull));
        //System.out.println("Trial Days: " + String.valueOf(trialdays));
        //System.out.println("Max Clients: " + String.valueOf(maxclients));
        //System.out.println("Product SKU: " + String.valueOf(productsku));
        //System.out.println("vend ID1: " + String.valueOf(vendid1));
        //System.out.println("vend ID2: " + String.valueOf(vendid2));
        
        if (firstauth >=234 && firstauth <=407) {
            _genuine = true;
        } else {            
            _genuine = false;
            //System.out.println("Invalid license key. Not genuine.");
            return false;
            
        }
        
        if (trialfull >= 0 && trialfull <= 77) {        
            _trial = true;
        }
        
        if (trialfull >= 78 && trialfull <= 99) {
            _trial = false;
        }
        
        _trialdays = trialdays;
        _sku = productsku;
        _max = maxclients;
        _vid1 = vendid1;
        _vid2 = vendid2;                
         
         return true;
    }
    
    /*
    Public Function HInsertEx(ByVal InString As String, ByVal SepLength As Integer) As String

        Dim ResultString As String = ""

        Dim C As Integer
        Dim HCount As Integer = 0


        For C = 1 To Len(InString)

            ResultString = ResultString + Mid(InString, C, 1)
            HCount = HCount + 1

            If HCount > SepLength Then
                ResultString = ResultString + "-"
                HCount = 0
            End If

        Next

        Return ResultString
    End Function
     */
    
    public String HInsertEx(String InString, int SepLength)
    {
        String ResultString = "";
        int c = 0;
        int HCount = 0;
        
        for (c=0;c<InString.length();c++) {
            ResultString+=InString.substring(c, c+1);
            HCount++;
            
            if (HCount > SepLength) {
                ResultString+= "-";
                HCount = 0;
            }
        }
        
        return ResultString;
    }
    
    public static  int GetRand (int Min, int Max)
    {
        int m = _rnd.nextInt(Max);
        
        return m+Min;
    }
    
    /*
        Public Function ResponseNumericToKey(ByVal strNumeric As String, ByVal ResponseCode As Integer) As String
        ' Called by the mobile device to test which response code was provided by the product activation page.
        ' The mobile device must call this function for each response code and compare the output to the
        ' original product key to determine which response code was provided by the web page.
        
        ' First get the random component        
        Dim iRandom As Integer
        Dim strReconkey As String = ""
        
        iRandom = ResponseCode
                
        Dim strRemaining As String
        strRemaining = strNumeric.Replace("-", "")
                        
        Dim A As Integer
        Dim strCur As String
        Dim iCur As Integer
        
        For A = 1 To Len(strRemaining) Step 3
            strCur = Mid(strRemaining, A, 3)
            iCur = Int32.Parse(strCur)
            iCur = iCur - iRandom
            strReconkey = strReconkey + Chr(iCur).ToString()
        Next
        
        Return strReconkey
    End Function
    */
    
    public String ResponseNumericToKey(String strNumeric, int ResponseCode)
    {
        int iRandom = 0;
        String strReconkey = "";
        
        byte[] bRecon = new byte[255];
        
        iRandom = ResponseCode;
        
        String strRemaining = "";
        strRemaining = StringUtilities.removeChars(strNumeric, "-");
        
        int a = 0;
        String strCur = "";
        int iCur = 0;
        int iPointer = 0;
        
        for (a=0;a<strRemaining.length();a+=3) {
            if ((a+3) <= strRemaining.length()) {
                strCur = strRemaining.substring(a, a+3);
                iCur = Integer.parseInt(strCur);
                iCur-=iRandom;
                bRecon[iPointer] = (byte) iCur;
                
                iPointer++;
            }
        }
        
        byte[] bReconex = new byte[iPointer];

        iPointer = 0;
        for (a=0;a<strRemaining.length();a+=3) {
            if ((a+3) <= strRemaining.length()) {
                strCur = strRemaining.substring(a, a+3);
                iCur = Integer.parseInt(strCur);
                iCur-=iRandom;
                bReconex[iPointer] = (byte) iCur;
                
                iPointer++;
            }
        }
        
        strReconkey = new String(bReconex);
        return strReconkey;
    }
    
    public String GenerateRequestCode (String strProductkey)
    {
        String strNumeric = "";
        int a = 0;
        int iRand = 0;
        int iNum = 0;
        byte[] bProdkey = strProductkey.getBytes();
        
        iRand = GetRand(100, 400);
        
        String strRand = "";
        strRand = String.valueOf(iRand);
        
        for (a=0;a<strProductkey.length();a++) {
            iNum = bProdkey[a];
            iNum+=iRand;
            strNumeric+=String.valueOf(iNum);
        }
        
        strNumeric = strRand + strNumeric;
        strNumeric = HInsertEx(strNumeric, 4);
        
        return strNumeric;
    }    
    
    
    private boolean initializeTransportAvailability() {
        ServiceBook sb = ServiceBook.getSB();
        ServiceRecord[] records = sb.getRecords();
        boolean res = false;

        for (int i = 0; i < records.length; i++) {
                ServiceRecord myRecord = records[i];
                String cid, uid;

                if (myRecord.isValid() && !myRecord.isDisabled()) {
                        cid = myRecord.getCid().toLowerCase();
                        uid = myRecord.getUid().toLowerCase();
                        // Wap2.0
                        if (cid.indexOf("wptcp") != -1 && uid.indexOf("wifi") == -1 && uid.indexOf("mms") == -1) {
                                srWAP2 = myRecord;
                                res = true;
                        }                        
                }       
        }
        
        return res;                                                
    }
    
    private String getWAP2URL(String baseURL) {
                return baseURL+ ";deviceside=true" + ";ConnectionUID=" + srWAP2.getUid();
    }
    
    public String AutoActivate(String url) throws IOException {
         String actualurl = "";
         
         if (initializeTransportAvailability() == true) {
            actualurl = getWAP2URL(url);   
         } else {
            return "";
         }
                           
         StreamConnection c = null;
         InputStream s = null;
         byte[] bData = new byte[255];
         int ipointer = 0;
         String strData = "";
         try {
             c = (StreamConnection)Connector.open(actualurl);
             s = c.openInputStream();
             int ch;
             while ((ch = s.read()) != -1) {
                 //...
                if (ipointer < 255) {
                    bData[ipointer] = (byte) ch;
                    ipointer++;
                }
             }
             
             strData = new String(bData);
             
         } finally {
             if (s != null) {
                 s.close();   
             }
                           
             if (c != null) {
                 c.close();
             }
             
            
         }
         
         return strData;
     }


     public String AutoActivateEx(String url) throws IOException {
         ContentConnection c = null;
         DataInputStream is = null;
         byte[] bData = new byte[255];
         int ipointer = 0;
         String strData = "";
         
         try {
             c = (ContentConnection)Connector.open(url);
             int len = (int)c.getLength();
             is = c.openDataInputStream();
             if (len > 0) {
                 byte[] data = new byte[len];
                 is.readFully(data);
             } else {
                 int ch;
                 while ((ch = is.read()) != -1) {
                     //...
                    if (ipointer < 255) {
                        bData[ipointer] = (byte) ch;
                        ipointer++;
                    }
                 }
                 
                strData = new String(bData);
               
             }
         } finally {
             if (is != null) {
                 is.close();
             }             
             if (c != null) {
                 c.close();
            }
            
            
         }
         
          return strData;
     }

    
    Licensing() {    
    
    }
} 
