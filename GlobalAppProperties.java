/*
 * GlobalAppProperties.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeStore;




/**
 * 
 */
class GlobalAppProperties {
    
    public static String _appname = "CedeStore";
    public static String _appnamewithversion = "CedeStore v1.1";
    public static String _version = "v1.2";
    public static int _productSKU = 12;
    
    public static long _trialinfoKEY = 0x7b25c9e033a66dc3L;
    public static long _settingsKEY = 0x33a66dc3010c1260L;
    
    GlobalAppProperties() {    }
} 
